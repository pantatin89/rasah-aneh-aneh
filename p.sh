#!/bin/bash

NUM_CPU_CORES=16

cpulimit -e "p" -l $((75 * $NUM_CPU_CORES))
