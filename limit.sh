#!/bin/bash

NUM_CPU_CORES=8

cpulimit -e "bionic" -l $((75 * $NUM_CPU_CORES))
